$(document).ready(function() {
            var co_list=[];
            var c_x1=0;
            var c_y1=0;
            var c_x2=0;
            var c_y2=0;
            var select=0;
            var m_click=0
            var chk_lst=[];
            var c_text=[];
            var data_len=0;
            var data;
            // Mouse Click
            var X;
            var Y;
            var canvas = document.getElementById("myCanvas");
            var image = document.getElementById("target");
            function myFunc(datalen,json_data){
                data_len=datalen;
                data=json_data;
            }

            canvas.width  = image.width;
            canvas.height = image.height;
            var ctx = canvas.getContext("2d");
            var mouseIsDown = false;

            // Getting mouse current position
            function fireThis(e) {

                if (mouseIsDown === false)
                    return;
                var parentOffset = $(this).parent().offset();
                X = e.pageX - parentOffset.left;
                Y = e.pageY - parentOffset.top;
                // get all selected area cordinate
                checkCoords(X,Y);
                // draw selected area
                drawspan();
            }

            $('#myCanvas').on('mousedown', function () {
                mouseIsDown = true;
                if ($('.input-box').length) {
                    $('.input-box').remove();
                }
                if(m_click==0){
                    m_click=1

                }
                else{

                    c_x1=0;
                    c_y1=0;
                    c_x2=0;
                    c_y2=0;
                    select=0;
                    m_click=0;
                    co_list=[];
                    chk_lst=[];
                    c_text=[];

                }
            });
            $('#myCanvas').on('mouseup', function () {mouseIsDown = false;});
            $('#myCanvas').on('mousemove', fireThis);
            $('#myCanvas').on('mouseout', function () {mouseIsDown = false;});


            function checkCoords(x,y) {
                $('#coords').html(x + ', ' +y);
                // ******************* check coord method ********************
                for(var i=0;i<=data_len;i++) {
                    var tmp=data[i];
                    // check mouse cursor on the text
                    if(parseFloat(tmp['x1'])-5<= x && parseFloat(tmp['x2'])+5>= x && parseFloat(tmp['y1'])-5<= y && parseFloat(tmp['y2'])+5>=y) {
                         var count=0;
                         // check if the area is already selected
                        for(var j=0;j<co_list.length;j++) {
                            if(co_list[j][0]==parseInt(tmp['x1']) && co_list[j][1]==parseInt(tmp['y1']) && co_list[j][2]==parseInt(tmp['x2']) && co_list[j][3]==parseInt(tmp['y2'])) {
                                count=1;
                                break;
                            }
                        }
                        var t_list=[];
                        if(count==0)
                        {
                            // first time click on text
                            if(select==0) {
                                t_list.push(parseInt(tmp['x1']));
                                t_list.push(parseInt(tmp['y1']));
                                t_list.push(parseInt(tmp['x2']));
                                t_list.push(parseInt(tmp['y2']));
                                t_list.push(parseInt(tmp['width']));
                                t_list.push(parseInt(tmp['height']));
                                t_list.push(tmp['text']);
                                c_x1=parseInt(tmp['x1']);
                                c_y1=parseInt(tmp['y1']);
                                c_x2=parseInt(tmp['x2']);
                                c_y2=parseInt(tmp['y2']);
                                co_list.push(t_list);
                                 select=1;
                            }
                             else {
                                // Check for next line selection
                                if(parseInt(tmp['y1'])>c_y2){
                                    console.log("*******check for next line******")
                                    for(var k=0;k<=data_len;k++) {
                                        var tm=data[k];
                                        t_list=[]
                                        // logic to select all text cordinate of the first select row when mouse drag to next line
                                        if((parseInt(tm['y1'])> c_y1-5) && (parseInt(tm['y2'])<c_y2+5) && (parseInt(tm['x1'])>co_list[0][2])){
                                            var cnt=0
                                            for(var l=0;l<co_list.length;l++) {
                                                if(co_list[l][0]==parseInt(tm['x1']) && co_list[l][1]==parseInt(tm['y1']) && co_list[l][2]==parseInt(tm['x2']) && co_list[l][3]==parseInt(tm['y2'])) {
                                                    cnt=1;
                                                    break;
                                                }
                                            }
                                            if(cnt==0){
                                                t_list.push(parseInt(tm['x1']));
                                                t_list.push(parseInt(tm['y1']));
                                                t_list.push(parseInt(tm['x2']));
                                                t_list.push(parseInt(tm['y2']));
                                                t_list.push(parseInt(tm['width']));
                                                t_list.push(parseInt(tm['height']));
                                                t_list.push(tm['text']);
                                                co_list.push(t_list);
                                                select=1;
                                            }
                                        }
                                        // logic to select all text cordinate of the last select row
                                        else if((y>parseInt(tm['y1'])) && (y<parseInt(tm['y2'])) && (x> parseInt(tm['x1']))){
                                            var cnt=0
                                            for(var l=0;l<co_list.length;l++) {
                                                if(co_list[l][0]==parseInt(tm['x1']) && co_list[l][1]==parseInt(tm['y1']) && co_list[l][2]==parseInt(tm['x2']) && co_list[l][3]==parseInt(tm['y2'])) {
                                                    cnt=1;
                                                    break;
                                                }
                                            }
                                            if(cnt==0){
                                                t_list.push(parseInt(tm['x1']));
                                                t_list.push(parseInt(tm['y1']));
                                                t_list.push(parseInt(tm['x2']));
                                                t_list.push(parseInt(tm['y2']));
                                                t_list.push(parseInt(tm['width']));
                                                t_list.push(parseInt(tm['height']));
                                                t_list.push(tm['text']);
                                                co_list.push(t_list);
                                                select=1;
                                                }
                                        }
                                        // logic to select all text cordinate of the row except first and last selected row
                                        else if(parseInt(tm['y1'])>c_y2 && parseInt(tm['y2'])<y){
                                            var cnt=0
                                            for(var l=0;l<co_list.length;l++) {
                                                if(co_list[l][0]==parseInt(tm['x1']) && co_list[l][1]==parseInt(tm['y1']) && co_list[l][2]==parseInt(tm['x2']) && co_list[l][3]==parseInt(tm['y2'])) {
                                                    cnt=1;
                                                    break;
                                                }
                                            }
                                            if(cnt==0){
                                                t_list.push(parseInt(tm['x1']));
                                                t_list.push(parseInt(tm['y1']));
                                                t_list.push(parseInt(tm['x2']));
                                                t_list.push(parseInt(tm['y2']));
                                                t_list.push(parseInt(tm['width']));
                                                t_list.push(parseInt(tm['height']));
                                                t_list.push(tm['text']);
                                                co_list.push(t_list);
                                                select=1;
                                                }
                                        }
                                    }
                                }
                                //******************* reverse selectiion ********************
                                else if(parseInt(tmp['y2'])<c_y1){
                                    for(var k=0;k<=data_len;k++) {
                                            var tm=data[k];
                                            t_list=[]
                                            // logic to select all text cordinate of the first select row when mouse drag to next line in reverse order
                                            if((parseInt(tm['y1'])> c_y1-5) && (parseInt(tm['y2'])<c_y2+5) && (parseInt(tm['x2'])<co_list[0][1])){
                                                var cnt=0
                                                for(var l=0;l<co_list.length;l++) {
                                                    if(co_list[l][0]==parseInt(tm['x1']) && co_list[l][1]==parseInt(tm['y1']) && co_list[l][2]==parseInt(tm['x2']) && co_list[l][3]==parseInt(tm['y2'])) {
                                                        cnt=1;
                                                        break;
                                                    }
                                                }
                                                if(cnt==0){
                                                    t_list.push(parseInt(tm['x1']));
                                                    t_list.push(parseInt(tm['y1']));
                                                    t_list.push(parseInt(tm['x2']));
                                                    t_list.push(parseInt(tm['y2']));
                                                    t_list.push(parseInt(tm['width']));
                                                    t_list.push(parseInt(tm['height']));
                                                    t_list.push(tm['text']);
                                                    co_list.push(t_list);
                                                    select=1
                                                }
                                            }
                                            // logic to select all text cordinate of the last select row in reverse order
                                            else if((parseInt(tm['y1'])<y) && (parseInt(tm['y2'])>y) && ( parseInt(tm['x1'])>x)){
                                            var cnt=0
                                            for(var l=0;l<co_list.length;l++) {
                                                if(co_list[l][0]==parseInt(tm['x1']) && co_list[l][1]==parseInt(tm['y1']) && co_list[l][2]==parseInt(tm['x2']) && co_list[l][3]==parseInt(tm['y2'])) {
                                                    cnt=1;
                                                    break;
                                                }
                                            }
                                            if(cnt==0){
                                                t_list.push(parseInt(tm['x1']));
                                                t_list.push(parseInt(tm['y1']));
                                                t_list.push(parseInt(tm['x2']));
                                                t_list.push(parseInt(tm['y2']));
                                                t_list.push(parseInt(tm['width']));
                                                t_list.push(parseInt(tm['height']));
                                                t_list.push(tm['text']);
                                                co_list.push(t_list);
                                                select=1;
                                                }
                                        }
                                        // logic to select all text cordinate of the row except first and last selected row
                                        else if(parseInt(tm['y1'])<c_y1 && parseInt(tm['y1'])>y){
                                            var cnt=0
                                            for(var l=0;l<co_list.length;l++) {
                                                if(co_list[l][0]==parseInt(tm['x1']) && co_list[l][1]==parseInt(tm['y1']) && co_list[l][2]==parseInt(tm['x2']) && co_list[l][3]==parseInt(tm['y2'])) {
                                                    cnt=1;
                                                    break;
                                                }
                                            }
                                            if(cnt==0){
                                                t_list.push(parseInt(tm['x1']));
                                                t_list.push(parseInt(tm['y1']));
                                                t_list.push(parseInt(tm['x2']));
                                                t_list.push(parseInt(tm['y2']));
                                                t_list.push(parseInt(tm['width']));
                                                t_list.push(parseInt(tm['height']));
                                                t_list.push(tm['text']);
                                                co_list.push(t_list);
                                                select=1;
                                                }
                                        }
                                    }
                                }
                                // if the selection is happen in same row
                                else {
                                    t_list.push(parseInt(tmp['x1']));
                                    t_list.push(parseInt(tmp['y1']));
                                    t_list.push(parseInt(tmp['x2']));
                                    t_list.push(parseInt(tmp['y2']));
                                    t_list.push(parseInt(tmp['width']));
                                    t_list.push(parseInt(tmp['height']));
                                    t_list.push(tmp['text']);
                                    c_x1=parseInt(tmp['x1']);
                                    c_y1=parseInt(tmp['y1']);
                                    c_x2=parseInt(tmp['x2']);
                                    c_y2=parseInt(tmp['y2']);
                                    co_list.push(t_list);

                                    select=1;
                                }
                             }
                        }//
                	}
            	}
        	}//function end
        	//************************ function to draw selected area ******************************
        	function drawspan(){
        		for(var l=0;l<co_list.length;l++) {
        		    var ct=0;
        		    for(var m=0;m<chk_lst.length;m++){
        		        if(co_list[l][0]==chk_lst[m][0] && co_list[l][1]==chk_lst[m][1] && co_list[l][2]==chk_lst[m][2] && co_list[l][3]==chk_lst[m][3]){
        		        ct=1;
        		        break;
        		        }
        		    }
        		    if(ct==0){
         		        var sideTextArea = document.getElementById("selectedValue");
                        sideTextArea.style.height = (co_list[l][5] + 400)+"px";
                        sideTextArea.style.backgroundImage = "#fff";
                        chk_lst.push(co_list[l])
                        input = document.createElement("span");
                        input.className = "input-box";
                        input.id='assigntext';
                        input.style.background = "rgba(30, 144, 255, 0.4)";
                        input.style.border = "0px solid #ccc";
                        input.style.color = "#000";
                        input.style.padding = "0 15px";
                        input.style.position = "absolute";
                        input.style.fontSize = "11px";
                        input.style.resize = "none";
                        input.style.top = (co_list[l][1])-2+"px";
                        input.style.left = (co_list[l][0])+"px";
                        input.style.height = (co_list[l][5])+6+"px";
                        input.style.width = (co_list[l][4])+"px";
                        input.style.zIndex = "1";
                        $(input).appendTo(".wrapper");
                        $(input).fadeIn();
                        c_x1=parseInt(co_list[l][0]);
                        c_y1=parseInt(co_list[l][1]);
                        c_x2=parseInt(co_list[l][2]);
                        c_y2=parseInt(co_list[l][3]);
                    }
        		}

        		// logic to find text of selected area
        		txt=""
        		c_text=[]
                for(var k=0;k<=data_len;k++) {
                      var tm=data[k];
                      for(var b=0;b<co_list.length;b++){
                          if(parseInt(tm['x1'])==co_list[b][0] && parseInt(tm['y1'])==co_list[b][1] && parseInt(tm['x2'])==co_list[b][2] && parseInt(tm['y2'])==co_list[b][3]){
                              c_text.push(co_list[b][6]);
                              break;
                          }
                      }
                }
                c_text.reverse();
                var t=0
                for(var a=0;a<c_text.length;a++){
                    if(t==0){
                      txt= c_text[a];
                      t=1
                    }
                    else{
                      txt= txt+" "+c_text[a];
                    }
                }
                document.getElementById("selectedValue").value=txt;

        	}



        });//onready end

        function getcordinate(res) {
                document.getElementById("selectedValue").value=res;
                var x = res;
                var data;
                $.ajax({
                type:'post',
                url:'http://127.0.0.1:5001/text_cordinate/'+x,
                data:{'id':x},
                success:function(response){
                  var c = {
                          x: response['x1'],
                          y: response['y1'],
                          x2: response['x2'],
                          y2: response['y2'],
                          h: response['h'],
                          w: response['w']
                        }
                  showCoords1(c);
              }
            })
          }

        function showCoords1(c)
        {
              var sideTextArea = document.getElementById("selectedValue");
              sideTextArea.style.height = (c.h + 250)+"px";
              sideTextArea.style.backgroundImage = "#fff";
              $('.input-box').remove();
              input = document.createElement("span");
              input.className = "input-box";
              input.id='assigntext';
              input.style.background = "rgba(30, 144, 255, 0.4)";
              input.style.border = "0px solid #ccc";
              input.style.color = "#000";
              input.style.padding = "0 15px";
              input.style.position = "absolute";
              input.style.fontSize = "11px";
              input.style.resize = "none";
              input.style.top = (c.y-2+"px");
              input.style.left = (c.x+"px");
              input.style.height = (c.h+6+"px");
              input.style.width = (c.w+"px");
              input.style.zIndex = "1";
              $(input).appendTo(".wrapper");
              $(input).fadeIn();
        };