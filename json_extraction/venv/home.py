from flask import Flask, render_template, request, flash, redirect, url_for, jsonify
import json
from werkzeug.utils import secure_filename
import glob
import os
from flask_cors import CORS
app = Flask(__name__)
UPLOAD_FOLDER = 'static/Files/'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
CORS(app)

# api to load home page
@app.route('/home', methods=['GET', 'POST'])
def home():
    print("home api")
    if request.method == 'POST':
        print("file")
        if 'file' not in request.files:
            print("file not found")
            return "file not found"
        else:
            print("file found")
            file = request.files['file']
            filename = secure_filename(file.filename)
            # logic to remove previous file
            files = glob.glob('static/Files/*')
            for f in files:
                os.remove(f)
            # logic to upload an json file
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            file_name = filename
            # read json file
            print("filename=",file_name)
            # read json file
            with open('static/Files/'+file_name, encoding="utf8") as f:
                data = json.load(f)
                # print(data)
            file_path=[]
            dt=[]
            for i in data['pages']:

                # select only second page data
                if i["page_no"] == 2:
                    print(i['image_path'])
                    file_path.append(i['image_path'])
                    dt=i['page_text_item']
                    special_text=['Alcoholic','preceding','respectively','decreased']
                    print("dt=",dt)
            print("file path=",file_path)
            return render_template('show.html',path=file_path,special_text=special_text,data=dt,data_len=len(dt)-1)
    return render_template('index.html')


@app.route('/text_cordinate/<t>', methods=['GET', 'POST'])
def text_cordinate(t):
    with open('static/Files/SouthKorea_July2018.json', encoding="utf8") as f:
        data = json.load(f)
    # print(data)
    rs = data["pages"]
    # r=rs["page_text_item"]
    # print(rs)
    cordinate = []
    for i in rs:
        print("###############################")
        # print(i["page_text_item"])
        if i["page_no"]==2:
            for j in i["page_text_item"]:
                if t==j["text"]:
                    cordinate.append(int(j["x1"]))
                    cordinate.append(int(j["y1"]))
                    cordinate.append(int(j["x2"]))
                    cordinate.append(int(j["y2"]))
                    cordinate.append(int(j["height"]))
                    cordinate.append(int(j["width"]))
                    cordinate.append((j["text"]))
                    break
    print("cordinate=",cordinate)
    return jsonify({'x1': cordinate[0],
                    'y1':cordinate[1],
                    'x2':cordinate[2],
                    'y2':cordinate[3],
                    'h':cordinate[4],
                    'w':cordinate[5],
                    't':cordinate[6]
                    })
if __name__ == "__main__":

    app.run(host='0.0.0.0', port='5001')